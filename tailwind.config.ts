import { skeleton, type CustomThemeConfig } from '@skeletonlabs/tw-plugin';
import { join } from 'node:path';
import type { Config } from 'tailwindcss';

const eurofursTheme: CustomThemeConfig = {
	name: 'eurofurs',
	properties: {
		// =~= Theme Properties =~=
		'--theme-font-family-base': "'Quicksand', sans-serif",
		'--theme-font-family-heading': "'Quicksand', sans-serif",
		'--theme-font-color-base': 'var(--color-surface-900)',
		'--theme-font-color-dark': 'var(--color-tertiary-50)',
		'--theme-rounded-base': '9999px',
		'--theme-rounded-container': '24px',
		'--theme-border-base': '3px',
		// =~= Theme On-X Colors =~=
		'--on-primary': '0 0 0',
		'--on-secondary': '0 0 0',
		'--on-tertiary': '0 0 0',
		'--on-success': '0 0 0',
		'--on-warning': '0 0 0',
		'--on-error': '0 0 0',
		'--on-surface': '0 0 0',
		// =~= Theme Colors  =~=
		// primary | #f2bc00
		'--color-primary-50': '253 245 217', // #fdf5d9
		'--color-primary-100': '252 242 204', // #fcf2cc
		'--color-primary-200': '252 238 191', // #fceebf
		'--color-primary-300': '250 228 153', // #fae499
		'--color-primary-400': '246 208 77', // #f6d04d
		'--color-primary-500': '242 188 0', // #f2bc00
		'--color-primary-600': '218 169 0', // #daa900
		'--color-primary-700': '182 141 0', // #b68d00
		'--color-primary-800': '145 113 0', // #917100
		'--color-primary-900': '119 92 0', // #775c00
		// secondary | #8893df
		'--color-secondary-50': '237 239 250', // #edeffa
		'--color-secondary-100': '231 233 249', // #e7e9f9
		'--color-secondary-200': '225 228 247', // #e1e4f7
		'--color-secondary-300': '207 212 242', // #cfd4f2
		'--color-secondary-400': '172 179 233', // #acb3e9
		'--color-secondary-500': '136 147 223', // #8893df
		'--color-secondary-600': '122 132 201', // #7a84c9
		'--color-secondary-700': '102 110 167', // #666ea7
		'--color-secondary-800': '82 88 134', // #525886
		'--color-secondary-900': '67 72 109', // #43486d
		// tertiary | #bdc3ee
		'--color-tertiary-50': '245 246 252', // #f5f6fc
		'--color-tertiary-100': '242 243 252', // #f2f3fc
		'--color-tertiary-200': '239 240 251', // #eff0fb
		'--color-tertiary-300': '229 231 248', // #e5e7f8
		'--color-tertiary-400': '209 213 243', // #d1d5f3
		'--color-tertiary-500': '189 195 238', // #bdc3ee
		'--color-tertiary-600': '170 176 214', // #aab0d6
		'--color-tertiary-700': '142 146 179', // #8e92b3
		'--color-tertiary-800': '113 117 143', // #71758f
		'--color-tertiary-900': '93 96 117', // #5d6075
		// success | #a0c60a
		'--color-success-50': '241 246 218', // #f1f6da
		'--color-success-100': '236 244 206', // #ecf4ce
		'--color-success-200': '231 241 194', // #e7f1c2
		'--color-success-300': '217 232 157', // #d9e89d
		'--color-success-400': '189 215 84', // #bdd754
		'--color-success-500': '160 198 10', // #a0c60a
		'--color-success-600': '144 178 9', // #90b209
		'--color-success-700': '120 149 8', // #789508
		'--color-success-800': '96 119 6', // #607706
		'--color-success-900': '78 97 5', // #4e6105
		// warning | #f99729
		'--color-warning-50': '254 239 223', // #feefdf
		'--color-warning-100': '254 234 212', // #feead4
		'--color-warning-200': '254 229 202', // #fee5ca
		'--color-warning-300': '253 213 169', // #fdd5a9
		'--color-warning-400': '251 182 105', // #fbb669
		'--color-warning-500': '249 151 41', // #f99729
		'--color-warning-600': '224 136 37', // #e08825
		'--color-warning-700': '187 113 31', // #bb711f
		'--color-warning-800': '149 91 25', // #955b19
		'--color-warning-900': '122 74 20', // #7a4a14
		// error | #ea5e3d
		'--color-error-50': '252 231 226', // #fce7e2
		'--color-error-100': '251 223 216', // #fbdfd8
		'--color-error-200': '250 215 207', // #fad7cf
		'--color-error-300': '247 191 177', // #f7bfb1
		'--color-error-400': '240 142 119', // #f08e77
		'--color-error-500': '234 94 61', // #ea5e3d
		'--color-error-600': '211 85 55', // #d35537
		'--color-error-700': '176 71 46', // #b0472e
		'--color-error-800': '140 56 37', // #8c3825
		'--color-error-900': '115 46 30', // #732e1e
		// surface | #b4bbea
		'--color-surface-50': '244 245 252', // #f4f5fc
		'--color-surface-100': '240 241 251', // #f0f1fb
		'--color-surface-200': '236 238 250', // #eceefa
		'--color-surface-300': '225 228 247', // #e1e4f7
		'--color-surface-400': '203 207 240', // #cbcff0
		'--color-surface-500': '180 187 234', // #b4bbea
		'--color-surface-600': '162 168 211', // #a2a8d3
		'--color-surface-700': '135 140 176', // #878cb0
		'--color-surface-800': '108 112 140', // #6c708c
		'--color-surface-900': '88 92 115' // #585c73
	},
	properties_dark: {
		// =~= Theme On-X Colors =~=
		'--on-primary': '0 0 0',
		'--on-secondary': '255 255 255',
		'--on-tertiary': '255 255 255',
		'--on-success': '0 0 0',
		'--on-warning': '0 0 0',
		'--on-error': '255 255 255',
		'--on-surface': '255 255 255',
		// =~= Theme Colors  =~=
		// primary | #ffc600
		'--color-primary-50': '255 246 217', // #fff6d9
		'--color-primary-100': '255 244 204', // #fff4cc
		'--color-primary-200': '255 241 191', // #fff1bf
		'--color-primary-300': '255 232 153', // #ffe899
		'--color-primary-400': '255 215 77', // #ffd74d
		'--color-primary-500': '255 198 0', // #ffc600
		'--color-primary-600': '230 178 0', // #e6b200
		'--color-primary-700': '191 149 0', // #bf9500
		'--color-primary-800': '153 119 0', // #997700
		'--color-primary-900': '125 97 0', // #7d6100
		// secondary | #6571ca
		'--color-secondary-50': '232 234 247', // #e8eaf7
		'--color-secondary-100': '224 227 244', // #e0e3f4
		'--color-secondary-200': '217 220 242', // #d9dcf2
		'--color-secondary-300': '193 198 234', // #c1c6ea
		'--color-secondary-400': '147 156 218', // #939cda
		'--color-secondary-500': '101 113 202', // #6571ca
		'--color-secondary-600': '91 102 182', // #5b66b6
		'--color-secondary-700': '76 85 152', // #4c5598
		'--color-secondary-800': '61 68 121', // #3d4479
		'--color-secondary-900': '49 55 99', // #313763
		// tertiary | #2f3988
		'--color-tertiary-50': '224 225 237', // #e0e1ed
		'--color-tertiary-100': '213 215 231', // #d5d7e7
		'--color-tertiary-200': '203 206 225', // #cbcee1
		'--color-tertiary-300': '172 176 207', // #acb0cf
		'--color-tertiary-400': '109 116 172', // #6d74ac
		'--color-tertiary-500': '47 57 136', // #2f3988
		'--color-tertiary-600': '42 51 122', // #2a337a
		'--color-tertiary-700': '35 43 102', // #232b66
		'--color-tertiary-800': '28 34 82', // #1c2252
		'--color-tertiary-900': '23 28 67', // #171c43
		// success | #b4d823
		'--color-success-50': '244 249 222', // #f4f9de
		'--color-success-100': '240 247 211', // #f0f7d3
		'--color-success-200': '236 245 200', // #ecf5c8
		'--color-success-300': '225 239 167', // #e1efa7
		'--color-success-400': '203 228 101', // #cbe465
		'--color-success-500': '180 216 35', // #b4d823
		'--color-success-600': '162 194 32', // #a2c220
		'--color-success-700': '135 162 26', // #87a21a
		'--color-success-800': '108 130 21', // #6c8215
		'--color-success-900': '88 106 17', // #586a11
		// warning | #ea8c22
		'--color-warning-50': '252 238 222', // #fceede
		'--color-warning-100': '251 232 211', // #fbe8d3
		'--color-warning-200': '250 226 200', // #fae2c8
		'--color-warning-300': '247 209 167', // #f7d1a7
		'--color-warning-400': '240 175 100', // #f0af64
		'--color-warning-500': '234 140 34', // #ea8c22
		'--color-warning-600': '211 126 31', // #d37e1f
		'--color-warning-700': '176 105 26', // #b0691a
		'--color-warning-800': '140 84 20', // #8c5414
		'--color-warning-900': '115 69 17', // #734511
		// error | #d04322
		'--color-error-50': '248 227 222', // #f8e3de
		'--color-error-100': '246 217 211', // #f6d9d3
		'--color-error-200': '243 208 200', // #f3d0c8
		'--color-error-300': '236 180 167', // #ecb4a7
		'--color-error-400': '222 123 100', // #de7b64
		'--color-error-500': '208 67 34', // #d04322
		'--color-error-600': '187 60 31', // #bb3c1f
		'--color-error-700': '156 50 26', // #9c321a
		'--color-error-800': '125 40 20', // #7d2814
		'--color-error-900': '102 33 17', // #662111
		// surface | #111a5e
		'--color-surface-50': '219 221 231', // #dbdde7
		'--color-surface-100': '207 209 223', // #cfd1df
		'--color-surface-200': '196 198 215', // #c4c6d7
		'--color-surface-300': '160 163 191', // #a0a3bf
		'--color-surface-400': '88 95 142', // #585f8e
		'--color-surface-500': '17 26 94', // #111a5e
		'--color-surface-600': '15 23 85', // #0f1755
		'--color-surface-700': '13 20 71', // #0d1447
		'--color-surface-800': '10 16 56', // #0a1038
		'--color-surface-900': '8 13 46' // #080d2e
	}
};

export default {
	darkMode: 'class',

	content: [
		'./src/**/*.{html,js,svelte,ts}',
		join(require.resolve('@skeletonlabs/skeleton'), '../**/*.{html,js,svelte,ts}')
	],

	theme: {
		extend: {}
	},

	plugins: [
		skeleton({
			themes: {
				custom: [eurofursTheme]
			}
		})
	]
} satisfies Config;
