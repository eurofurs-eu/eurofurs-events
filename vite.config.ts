import tailwindcss from 'tailwindcss';
import autoprefixer from 'autoprefixer';
import { purgeCss } from 'vite-plugin-tailwind-purgecss';
import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vitest/config';

export default defineConfig({
	plugins: [sveltekit(), purgeCss()],
	css: {
		postcss: {
			plugins: [tailwindcss(), autoprefixer]
		}
	},
	test: {
		include: ['src/**/*.{test,spec}.{js,ts}']
	}
});
