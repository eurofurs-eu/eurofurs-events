await Deno.writeTextFile(
	'build/handler.js',
	`import process from "node:process";\n${await Deno.readTextFile('build/handler.js')}`
);

await Deno.writeTextFile(
	'build/env.js',
	`import process from "node:process";\n${await Deno.readTextFile('build/env.js')}`
);

await Deno.writeTextFile(
	'build/index.js',
	`import { setImmediate } from 'node:timers';\n${await Deno.readTextFile('build/index.js')}`
);
