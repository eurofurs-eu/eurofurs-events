import type { EventCardData } from '$lib/components/event-card.svelte';
import { api } from '$lib/server/api';
import { mapEventCard } from '$lib/server/mapping/event';
import type { PageServerLoad } from './$types';

export const load: PageServerLoad = async ({ fetch }) => {
	const events = (await api(fetch).events()).map(mapEventCard).sort((a, b) => {
		if (!a.from) {
			if (!b.from) {
				return 0;
			}

			return 1;
		}

		if (!b.from) {
			return -1;
		}

		const [aFrom, bFrom] = [new Date(a.from), new Date(b.from)];

		if (aFrom < bFrom) {
			return -1;
		}

		return aFrom === bFrom ? 0 : 1;
	});

	const now = new Date();

	const [current, upcoming, past] = [[], [], []] as EventCardData[][];

	for (const event of events) {
		if (!event.from) {
			if (!event.to) {
				current.push(event);

				continue;
			}

			const to = new Date(event.to);

			if (now <= to) {
				current.push(event);
			} else {
				past.push(event);
			}

			continue;
		}

		const from = new Date(event.from);

		if (!event.to) {
			if (now >= from) {
				current.push(event);
			} else {
				upcoming.push(event);
			}

			continue;
		}

		const to = new Date(event.to);

		if (now >= from) {
			if (now <= to) {
				current.push(event);
			} else {
				past.push(event);
			}

			continue;
		}

		upcoming.push(event);
	}

	return {
		current,
		upcoming,
		past
	};
};
