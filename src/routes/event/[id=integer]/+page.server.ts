import { api } from '$lib/server/api';
import { error } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';
import { statusMap, typeMap } from '$lib/server/mapping/event';

export const load: PageServerLoad = async ({ params }) => {
	const event = await api(fetch).event(parseInt(params.id, 10));

	if (!event) {
		throw error(404, 'Not Found');
	}

	return {
		event: {
			id: event.id,
			type: typeMap[event.type],
			name: event.name,
			from: event.date_start,
			to: event.date_end,
			banner: event.banner,
			description: event.description,
			attendees: event.attendees,
			cost: event.cost,
			international: event.international,
			registration: {
				message: event.registration.message,
				open: event.registration.open,
				status: statusMap[event.registration.status],
				url: event.registration.url
			},
			venue: event.venue,
			links: event.links,
			organizer: event.organizer
		}
	};
};
