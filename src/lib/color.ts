export const hexToRgb = (hex: string): [number, number, number] | null => {
	const match = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

	return match ? [parseInt(match[1], 16), parseInt(match[2], 16), parseInt(match[3], 16)] : null;
};

export const fgCol = <T>(bg: [number, number, number], dark: T, bright: T): T =>
	Math.round((bg[0] * 299 + bg[1] * 587 + bg[2] * 114) / 1000) > 128 ? dark : bright;
