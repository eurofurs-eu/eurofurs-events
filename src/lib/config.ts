import { env } from '$env/dynamic/public';

export const PROJECT_NAME = env.PUBLIC_PROJECT_NAME || 'Eurofurs Events';
