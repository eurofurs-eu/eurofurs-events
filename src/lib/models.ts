export const enum EventType {
	Convention,
	SingleDay,
	Recurring
}

export const eventTypeNames = {
	[EventType.Convention]: 'Convention',
	[EventType.Recurring]: 'Regular meet',
	[EventType.SingleDay]: 'Single day event'
};

export const enum RegistrationStatus {
	NoReg,
	Unknown,
	Closed,
	Open
}

export const regStatusNames = {
	[RegistrationStatus.NoReg]: 'No registration',
	[RegistrationStatus.Unknown]: 'Unknown',
	[RegistrationStatus.Closed]: 'Closed',
	[RegistrationStatus.Open]: 'Open'
};
