import type { EventCardData } from '$lib/components/event-card.svelte';
import { EventType, RegistrationStatus } from '$lib/models';
import {
	EventType as ServerEventType,
	RegistrationStatus as ServerRegistrationStatus,
	type Event
} from '$lib/server/api';

export const typeMap = {
	[ServerEventType.Convention]: EventType.Convention,
	[ServerEventType.Recurring]: EventType.Recurring,
	[ServerEventType.SingleDay]: EventType.SingleDay
};

export const statusMap = {
	[ServerRegistrationStatus.NoReg]: RegistrationStatus.NoReg,
	[ServerRegistrationStatus.Unknown]: RegistrationStatus.Unknown,
	[ServerRegistrationStatus.Closed]: RegistrationStatus.Closed,
	[ServerRegistrationStatus.Open]: RegistrationStatus.Open
};

export const mapEventCard = (event: Event): EventCardData => ({
	id: event.id,
	name: event.name,
	type: typeMap[event.type],
	from: event.date_start,
	to: event.date_end,
	organizer: event.organizer.name,
	attendees: event.attendees,
	cost: event.cost,
	international: event.international,
	registration: {
		message: event.registration.message,
		opensAt: event.registration.open,
		status: statusMap[event.registration.status]
	},
	venue: {
		name: event.venue.name,
		city: event.venue.city,
		country: event.venue.country.name
	},
	links: event.links.map((link) => ({ name: link.name, url: link.url })),
	color: event.organizer.brand_color
});
