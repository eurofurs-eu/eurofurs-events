import { env } from '$env/dynamic/private';

export interface Country {
	name: string;
	code: string;
	emoji: string;
}

export interface Organizer {
	name: string;
	short_name: string | null;
	brand_color: string | null;
	url: string | null;
	announcements: string | null;
	country: Country;
	logo: string | null;
}

export const enum EventType {
	Convention = 'Convention',
	SingleDay = 'SingleDay',
	Recurring = 'Recurring'
}

export const enum RegistrationStatus {
	NoReg = 'NoReg',
	Unknown = 'Unknown',
	Closed = 'Closed',
	Open = 'Open'
}

export interface Coordinates {
	lat: number | null;
	lon: number | null;
}

export interface Venue {
	name: string;
	address: string | null;
	country: Country;
	city: string;
	coords: Coordinates | null;
}

export interface Link {
	emoji: string;
	name: string;
	url: string;
}

export interface Event {
	id: number;
	type: EventType;
	name: string;
	date_start: string | null;
	date_end: string | null;
	banner: string | null;
	description: string | null;
	attendees: number;
	cost: number;
	international: boolean;
	registration: {
		message: string | null;
		open: string | null;
		status: RegistrationStatus;
		url: string | null;
	};
	venue: Venue;
	links: Link[];
	organizer: Organizer;
}

export const api = (fetch: typeof API.prototype.fetch): API => {
	if (!env.API_URL) {
		throw new Error('API_URL is required in environment');
	}

	return new API(new URL(env.API_URL), fetch);
};

export class API {
	fetch: (input: RequestInfo | URL, init?: RequestInit) => Promise<Response>;

	constructor(base: URL, fetchFunction: typeof API.prototype.fetch) {
		this.fetch = (input, init) => {
			if (input instanceof Request) {
				return fetchFunction(input, init);
			}

			return fetchFunction(new URL(input, base), init);
		};
	}

	async organizers(): Promise<Organizer[]> {
		const response = await this.fetch('organizers/');
		return await response.json();
	}

	async organizer(nameOrShortName: string): Promise<Organizer | null> {
		const response = await this.fetch(`organizers/${nameOrShortName}/`);

		if (response.status === 404) {
			return null;
		}

		return await response.json();
	}

	async events(): Promise<Event[]> {
		const response = await this.fetch('events/');
		return await response.json();
	}

	async event(id: number): Promise<Event | null> {
		const response = await this.fetch(`events/${id}/`);

		if (response.status === 404) {
			return null;
		}

		return await response.json();
	}
}
