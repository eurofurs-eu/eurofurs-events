export const dateFormat = new Intl.DateTimeFormat('en', {
	year: 'numeric',
	month: 'short',
	day: 'numeric'
});

export const dtFormat = new Intl.DateTimeFormat('en', {
	year: 'numeric',
	month: 'short',
	day: 'numeric',
	hour: '2-digit',
	minute: '2-digit',
	hour12: false,
	timeZoneName: 'short'
});

export const MILLISECOND = 1;
export const SECOND = 1000 * MILLISECOND;
export const MINUTE = 60 * SECOND;
export const HOUR = 60 * MINUTE;
export const UTC_DAY = 24 * HOUR;

export const date = (value: string): string => dateFormat.format(new Date(value));

export const dateTime = (value: string): string => dtFormat.format(new Date(value));

export const dateRange = (from: string | null, to: string | null): string => {
	if (from && to) {
		return dateFormat.formatRange(new Date(from), new Date(to));
	}

	if (from) {
		return `From ${dateFormat.format(new Date(from))}`;
	}

	if (to) {
		return `Until ${dateFormat.format(new Date(to))}`;
	}

	return 'Unknown';
};

export const dayCount = (from: string, to: string): number => {
	const [fromDate, toDate] = [new Date(from), new Date(to)];

	const diffMs = Math.abs(toDate.getTime() - fromDate.getTime());

	return Math.round(diffMs / UTC_DAY) + 1;
};

export const fmtUrl = (url: URL): string => {
	if (url.pathname === '/') {
		return url.hostname;
	}

	return `${url.hostname}${url.pathname}`;
};
